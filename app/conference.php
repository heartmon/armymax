<!DOCTYPE html>
<html>
<head>
	<title>Multiparty Chatroom</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="/easyrtc/easyrtc.css" />

	<meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
	<script type="text/javascript" src="/easyrtc/easyrtc.js"></script>
	<script src="/socket.io/socket.io.js"></script>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/demo10.css" />

	<script type="text/javascript" src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script type="text/javascript" src="js/demo10.js"></script>
</head>
	<body onload="appInit()">
		<div class="container-fluid">
			<div class="row no-gutter">
				<!-- Timeline -->
				<div id="timeline" class="col-xs-3">
					<!-- <iframe style="width:100%; height:700px; border:0;" src="https://schat.vdomax.com/?userID=11&userToken=asdf11&type=user&friendid=2"></iframe> -->
				</div>

				<!-- Video -->
				<div class="col-xs-9">
					<div id="conference_box" style="display: flex;  -webkit-flex-flow: row wrap;">
			            <video id="caller_video1" class="animate-spring transit boxCommon thumbCommon" style="display:none; visibility:hidden"></video>
			            <video id="caller_video2" class="animate-spring transit boxCommon thumbCommon" style="display:none; visibility:hidden"></video>
			            <video id="caller_video3" class="animate-spring transit boxCommon thumbCommon" style="display:none; visibility:hidden"></video>
			            <video id="caller_video4" class="animate-spring transit boxCommon thumbCommon" style="display:none; visibility:hidden"></video>
			            <video id="caller_video5" class="animate-spring transit boxCommon thumbCommon" style="display:none; visibility:hidden"></video>
			            <video id="caller_video6" class="animate-spring transit boxCommon thumbCommon" style="display:none; visibility:hidden"></video>
			            <video id="caller_video7" class="animate-spring transit boxCommon thumbCommon" style="display:none; visibility:hidden"></video>
			            <video id="caller_video8" class="animate-spring transit boxCommon thumbCommon" style="display:none; visibility:hidden"></video>
			            <video id="caller_video9" class="animate-spring transit boxCommon thumbCommon" style="display:none; visibility:hidden"></video>
		            </div>
					<!-- Your Panel -->
					<div class="row">	
						<!-- Menu Bar and Type Box -->
						<div class="col-xs-9">
							
						</div>
						<!-- Your Video -->
						<div class="col-xs-3">
							<video id="self_video" class="animate-spring transit boxCommon thumbCommon"  ></video>
						</div>
					</div>
				</div>
			</div>			
		</div>

    </body>
</html>